# nw-tinymce-templates

__This package is not full yet!__

This package was created for NovyWeb team. We use kohana core and tinymce editor inside our cms administration. This package contain universal templates for users and make easier implementation for us.

## Implementation

### FrontEnd

You need add link to header with global styles from __src/css/global-styles.css__

`<link rel="stylesheet" type="text/css" href="<urlsite>node_modules/@reckziegel/nw-tinymce-templates/src/css/global-styles.css">`

or

`Model_Template_Layout::addCss(URL::site('node_modules/@reckziegel/nw-tinymce-templates/src/css/global-styles.css'));`



### TinyMCE editor inside CMS

You need add this styles to editor with global styles from __src/css/global-styles-editor.css__ manually to __kohana/templates/<project>/media/css/editor.css__

Also you have to create config for tinymce editor and defined templates.



## Settings

Settings for templates are inside file __global-styles-default.scss__.



## To Do

1. Define tinymce templates only for specific web views.
2. Options of template inside tinymce editor.